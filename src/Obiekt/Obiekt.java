/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Obiekt;

import java.io.Serializable;
import java.math.BigInteger;

/**
 *
 * @author Mateusz
 */
public class Obiekt implements Serializable {

    private BigInteger wiadomosc;
    private BigInteger wiadomoscDodatkowa;
    private BigInteger wiadomoscDodatkowaBonus;
    private BigInteger tag;

    public Obiekt(String wiadomosc, int tag) {

        this.wiadomosc = convertStringToBigInteger(wiadomosc);
        this.tag = convertStringToBigInteger(String.valueOf(tag));
    }

    public Obiekt(int tag) {
        this.tag = convertStringToBigInteger(String.valueOf(tag));
    }

    public Obiekt(String wiadomosc) {
        this.wiadomosc = convertStringToBigInteger(wiadomosc);
    }

    public Obiekt(String wiadomosc, String wiadomoscDodatkowa, int tag) {
        this.wiadomosc = convertStringToBigInteger(wiadomosc);
        this.wiadomoscDodatkowa = convertStringToBigInteger(wiadomoscDodatkowa);
        this.tag = convertStringToBigInteger(String.valueOf(tag));
    }

    public Obiekt(String wiadomosc, String wiadomoscDodatkowa, Double wiadomoscDodatkowaBonus, int tag) {
        this.wiadomosc = convertStringToBigInteger(wiadomosc);
        this.wiadomoscDodatkowa = convertStringToBigInteger(wiadomoscDodatkowa);
        this.wiadomoscDodatkowaBonus = convertStringToBigInteger(String.valueOf(wiadomoscDodatkowaBonus));
        this.tag = convertStringToBigInteger(String.valueOf(tag));
    }

    public String getWiadomosc() {
        return convertBigIntegerToString(wiadomosc);

    }

    public String getWiadomoscDodatkowa() {
        return convertBigIntegerToString(wiadomoscDodatkowa);
    }

    public Double getWiadomoscDodatkowaBonus() {
        return Double.valueOf(convertBigIntegerToString(wiadomoscDodatkowaBonus));
    }

    public int getTag() {
        return Integer.valueOf(convertBigIntegerToString(tag));
    }

    public static BigInteger convertStringToBigInteger(String s) {
        BigInteger b = new BigInteger("0");
        for (int i = 0; i < s.length(); i++) {
            Integer code = new Integer((int) s.charAt(i));
            BigInteger c = new BigInteger(code.toString());
            b = b.shiftLeft(8);
            b = b.or(c);
        }
        return b;
    }

    public static String convertBigIntegerToString(BigInteger b) {
        String s = new String();
        while (b.compareTo(BigInteger.ZERO) == 1) {
            BigInteger c = new BigInteger("11111111", 2);
            int cb = (b.and(c)).intValue();
            Character cv = new Character((char) cb);
            s = (cv.toString()).concat(s);
            b = b.shiftRight(8);
        }
        return s;
    }

}
