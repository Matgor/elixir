package Elixir;

import java.sql.*;
import java.util.*;
import java.util.List;
import java.util.Arrays;

/**
 *
 * @author Marcin
 */
public class Sql {

    private final Connection con;
    private static Sql instance;

    private Sql(Connection con) {
        this.con = con;
    }

    public void disconnect() {
        try {
            con.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public static Sql getInstance() throws Exception {
        Connection con = null;
        try {
            System.out.println("Loading driver...");
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            System.out.println("Driver loaded!");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Cannot find the driver in the classpath!", e);
        }
        try {
            System.out.println("Connecting database...");
            con = DriverManager.getConnection("jdbc:mysql://sql5.freemysqlhosting.net:3306/sql543705", "sql543705", "zP1*yV7%");
            System.out.println("Database connected!");
        } catch (SQLException e) {
            throw new RuntimeException("Cannot connect the database!", e);
        }

        if (instance == null) {
            instance = new Sql(con);
        }
        return instance;

    }

    public ArrayList<Transaction> GetTransfer() {
        ArrayList<Transaction> lista = new ArrayList<Transaction>();
        try {
            Statement st = con.createStatement();
            String query = "SELECT * FROM Elixir";
            ResultSet rs = st.executeQuery(query);

            while (rs.next()) {
                Transaction t = new Transaction("", "", null);
                t.kwota = rs.getDouble("kwota");
                t.nadawca = rs.getString("nadawca");
                t.odbiorca = rs.getString("odbiorca");
                lista.add(t);

            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return lista;
    }

    public void updateTransfer(ArrayList<Transaction> lista) {

        try {
            Statement st = con.createStatement();

            for (Transaction item : lista) {

                if (item.nadawca.startsWith("1")) {

                    String query1 = "UPDATE bankB SET saldo = saldo + " + item.kwota + " WHERE login = " + item.odbiorca;
                    st.executeUpdate(query1);
                    String query2 = "UPDATE BankA SET zablokowane = zablokowane - " + item.kwota + " WHERE login = " + item.nadawca;
                    st.executeUpdate(query2);
                    String query3 = "UPDATE BankA SET history = CONCAT(history,'," + "Recipient: " + item.nadawca + " Receiver: " + item.odbiorca + " Kwota: " + item.kwota + "') WHERE login = '" + item.nadawca + "'";
                    st.executeUpdate(query3);
                    String query4 = "UPDATE bankB SET history = CONCAT(history,'," + "Recipient: " + item.nadawca + " Receiver: " + item.odbiorca + " Kwota: " + item.kwota + "') WHERE login = '" + item.nadawca + "'";
                    st.executeUpdate(query4);
                    String query5 = "UPDATE BankA SET saldo = saldo - " + item.kwota + " WHERE login = " + item.nadawca;
                    st.executeUpdate(query5);

                }
                if (item.nadawca.startsWith("2")) {
                    String query1 = "UPDATE BankA SET saldo = saldo + " + item.kwota + " WHERE login = " + item.odbiorca;
                    st.executeUpdate(query1);
                    String query2 = "UPDATE bankB SET zablokowane = zablokowane - " + item.kwota + " WHERE login = " + item.nadawca;
                    st.executeUpdate(query2);
                    String query3 = "UPDATE BankA SET history = CONCAT(history,'," + "Recipient: " + item.nadawca + " Receiver: " + item.odbiorca + " Kwota: " + item.kwota + "') WHERE login = '" + item.nadawca + "'";
                    st.executeUpdate(query3);
                    String query4 = "UPDATE bankB SET history = CONCAT(history,'," + "Recipient: " + item.nadawca + " Receiver: " + item.odbiorca + " Kwota: " + item.kwota + "') WHERE login = '" + item.nadawca + "'";
                    st.executeUpdate(query4);
                    String query5 = "UPDATE bankB SET saldo = saldo - " + item.kwota + " WHERE login = " + item.nadawca;

                    st.executeUpdate(query5);
                }

            }

            String queryDEL = "TRUNCATE TABLE Elixir";
            st.executeUpdate(queryDEL);

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

    }

    public String summaryA(ArrayList<Transaction> lista) {

        double out = 0;
        double in = 0;
        for (Transaction item : lista) {
            if (item.nadawca.startsWith("1")) {
                out = out + item.kwota;
            }
            if (item.odbiorca.startsWith("1")) {
                in = in + item.kwota;
            }
        }

        return "Summary of income to bank A: " + in + " and outcome:" + out;
    }

    public String summaryB(ArrayList<Transaction> lista) {

        double out = 0;
        double in = 0;
        for (Transaction item : lista) {
            if (item.nadawca.startsWith("2")) {
                out = out + item.kwota;
            }
            if (item.odbiorca.startsWith("2")) {
                in = in + item.kwota;
            }
        }

        return "Summary of income to bank B: " + in + " and outcome:" + out;
    }
}
