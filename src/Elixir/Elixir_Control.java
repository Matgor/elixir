/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Elixir;

import Obiekt.Obiekt;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Locale;
import java.util.Scanner;


/**
 *
 * @author Mateusz
 */
public class Elixir_Control {

    private static String SERVER_IP = "127.0.0.1";
    private static final int SERVER_PORT = 5000;
    private static String SERVER_IP2 = "127.0.0.1";
    private static final int SERVER_PORT2 = 6000;

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);;
        Socket socket = null;
        Socket socket2 = null;
        Sql sql = null;
        try {
            socket = new Socket(SERVER_IP, SERVER_PORT);
            socket2 = new Socket(SERVER_IP2, SERVER_PORT2);
            System.out.println("Servers connected");
        } catch (Exception ex) {
            System.out.println("Connection error: " + ex.getMessage());
        }

        ObjectInputStream in = null;
        ObjectOutputStream out = null;
        ObjectInputStream in2 = null;
        ObjectOutputStream out2 = null;
        while (true) {
            try {
                if (sql == null) {
                    sql = Sql.getInstance();
                }
                if (out == null) {
                    out = new ObjectOutputStream(socket.getOutputStream());
                }
                if (in == null) {
                    in = new ObjectInputStream(socket.getInputStream());
                }
                if (out2 == null) {
                    out2 = new ObjectOutputStream(socket2.getOutputStream());
                }
                if (in2 == null) {
                    in2 = new ObjectInputStream(socket2.getInputStream());
                }
                System.out.println();
                System.out.println("Options:");
                System.out.println("1 - start elixir session");
                System.out.println("2 - logout");
                int kom = scanner.nextInt();

                if (kom == 1) {
                    int tag = 5;
                    String komunikatA = sql.summaryA(sql.GetTransfer());
                    String komunikatB = sql.summaryB(sql.GetTransfer());
                  
                    out.writeObject(new Obiekt(komunikatA, tag));
                    out2.writeObject(new Obiekt(komunikatB, tag));
                    Obiekt wiad = (Obiekt) in.readObject();
                    Obiekt wiad2 = (Obiekt) in2.readObject();
                    
                    System.out.println(wiad.getWiadomosc());
                    System.out.println(wiad2.getWiadomosc());
                
                    sql.updateTransfer(sql.GetTransfer());
                }
                if (kom == 2) {
                    int tag = 4;
                    out.writeObject(new Obiekt(tag));
                    out2.writeObject(new Obiekt(tag));
                    Obiekt wiad = (Obiekt) in.readObject();
                    Obiekt wiad2 = (Obiekt) in2.readObject();

                    System.out.println(wiad.getWiadomosc());
                    System.out.println(wiad2.getWiadomosc());
                    System.exit(0);
                }

            } catch (Exception ex) {
                System.out.println("Error: " + ex);
            }
        }
    }
}