/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package BankB;
import BankB.*;
import java.util.*;
/**
 *
 * @author Marcin
 */
public class User {
    Integer id;
    Double saldo;
    String login;
    String password;
    String history;
    Double blocked;
    
     public User(Integer id, Double saldo, String login, String password,String history, Double blocked  )
    {
    this.id = id;
    this.saldo = saldo;
    this.login = login;
    this.password = password;
    this.history = history;
    this.blocked = blocked;
    }
}