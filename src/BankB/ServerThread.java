/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BankB;

import Obiekt.Obiekt;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

/**
 *
 * @author Mateusz
 */
public class ServerThread extends Thread {

    Socket socket = null;
    ObjectInputStream in = null;
    ObjectOutputStream out = null;
    Boolean running = true;
    User user = null;
    String komunikat = null;
    Sql sql = null;
    String IDbanku = "2";

    public ServerThread(Socket socket) {
        this.socket = socket;
    }

    public void CloseConnection(Socket s, ObjectInputStream in, ObjectOutputStream out) throws IOException {
        s.close();
        in.close();
        out.close();
        running = null;
        System.out.println("Customer " + user.login + " logged out");
    }

    @Override
    public void run() {
        while (running != null) {
            try {
                if (sql == null) {
                    sql = Sql.getInstance();
                }
                if (in == null) {
                    in = new ObjectInputStream(socket.getInputStream());
                }
                if (out == null) {
                    out = new ObjectOutputStream(socket.getOutputStream());
                }
                Obiekt wiad = (Obiekt) in.readObject();

                if (wiad.getTag() == 0) {

                    String login = wiad.getWiadomosc();
                    String haslo = wiad.getWiadomoscDodatkowa();
                    user = sql.searchUser(login);
                    if (user.login.equals(login) && user.password.equals(haslo)) {
                        komunikat = "Welcome!";
                        System.out.println("Customer " + user.login + " logged in.");
                    } else {
                        komunikat = "An error occurred, please try again";
                    }
                    out.writeObject(new Obiekt(komunikat, wiad.getTag()));
                    out.flush();
                }

                if (wiad.getTag() == 1) {
                    String login = wiad.getWiadomosc();
                    user = sql.searchUser(login);
                    String komunikat = "Your account balance is: " + user.saldo.toString() + " and blocked balance: " + user.blocked;;
                    out.writeObject(new Obiekt(komunikat));
                    out.flush();
                }

                if (wiad.getTag() == 2) {

                    String nadawca = wiad.getWiadomosc();
                    String odbiorca = wiad.getWiadomoscDodatkowa();
                    Double ilosc = wiad.getWiadomoscDodatkowaBonus();
                    User user1 = sql.searchUser(odbiorca);
                    User user11 = sql.searchUserA(odbiorca);
                    Double stan = user.saldo;
                    Double stanO = user1.saldo;
                    Double a = stan - ilosc - user.blocked;
                    Double b = stanO + ilosc;
                    if ("".equals(user1.login) && "".equals(user11.login)) {

                        komunikat = "User does not exist";
                        out.writeObject(new Obiekt(komunikat));
                        out.flush();

                    } else {
                        if (a < 0) {
                            komunikat = "You have not enough funds in your account";
                            out.writeObject(new Obiekt(komunikat));
                            out.flush();
                        } else {

                            if (odbiorca.startsWith(IDbanku)) {
                                if (!sql.updateSaldoIN(odbiorca, b)) {
                                    String komunikat = "User does not exist";
                                    out.writeObject(new Obiekt(komunikat));
                                    out.flush();
                                } else {
                                    sql.updateSaldoIN(nadawca, a);
                                    komunikat = "Transaction approved";
                                    System.out.println("Customer: " + user.login + " transfer " + ilosc + "to customer " + user1.login);
                                    sql.updateHistory(user.login, user1.login, ilosc, user1.login, user.login);
                                    out.writeObject(new Obiekt(komunikat));
                                    out.flush();

                                }
                            } else {

                                if (!sql.updateSaldoOUT(nadawca, odbiorca, b)) {
                                    String komunikat = "User does not exist";
                                    out.writeObject(new Obiekt(komunikat));
                                    out.flush();
                                } else {
                                    komunikat = "Transaction approved";
                                    System.out.println("Customer: " + user.login + " transfer " + ilosc + "to customer " + user1.login);
                                    out.writeObject(new Obiekt(komunikat));
                                    out.flush();

                                }

                            }

                        }

                    }
                }

                if (wiad.getTag() == 4) {
                    out.writeObject(new Obiekt("Logout"));
                    out.flush();
                    CloseConnection(socket, in, out);
                }
                if (wiad.getTag() == 3) {
                    String login = wiad.getWiadomosc();
                    user = sql.searchUser(login);
                    String komunikat = user.history;
                    System.out.println(komunikat);
                    out.writeObject(new Obiekt(komunikat));
                    out.flush();
                }
                if (wiad.getTag() == 5) {
                    String komunikat = wiad.getWiadomosc();
                    System.out.println(komunikat);
                    out.writeObject(new Obiekt("Bank B received a message"));
                    out.flush();

                }
            } catch (Exception ex) {
                System.out.println("Error: " + ex);
            }
        }
    }
}
