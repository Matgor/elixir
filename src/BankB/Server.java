/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BankB;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 * @author Mateusz
 */
public class Server {

    private static final int PORT = 6000;

    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket = null;

        serverSocket = new ServerSocket(PORT);

        Socket socket = null;

        while (true) {
            try {
                socket = serverSocket.accept();
                ServerThread st = new ServerThread(socket);
                st.start();
                System.out.println("Customer connected, client IP: " + socket.getInetAddress());
               
            } catch (IOException ex) {
                System.out.println(ex);
            }
        }
    }
}


