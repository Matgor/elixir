
    package BankB;
     
    
import java.sql.*;
import java.util.*;
import java.util.List;
import java.util.Arrays;

     
    /**
     *
     * @author Marcin edit Kuba
     */
    public class Sql {
     
        private final Connection con;
        private static Sql instance;
     
        private Sql(Connection con) {
            this.con = con;
        }
     
        public void disconnect() {
            try {
                con.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
     
        public static Sql getInstance() throws Exception{
            Connection con = null;
            try {
                System.out.println("Loading driver...");
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                System.out.println("Driver loaded!");
            } catch (ClassNotFoundException e) {
                throw new RuntimeException("Cannot find the driver in the classpath!", e);
            }
            try {
                System.out.println("Connecting database...");
             con = DriverManager.getConnection("jdbc:mysql://sql5.freemysqlhosting.net:3306/sql543705", "sql543705", "zP1*yV7%");
                System.out.println("Database connected!");
            } catch (SQLException e) {
                throw new RuntimeException("Cannot connect the database!", e);
            }
       
        if (instance == null) {
                instance = new Sql(con);
        }
        return instance ;
     
    }
     
    public void updateSaldo(String login, Double saldo) {
            try {
                Statement st = con.createStatement();
                String query = "UPDATE bankB SET saldo = '" + saldo + "'  WHERE login = '" + login + "'";
                st.executeUpdate(query);
               
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
     
     
        }
        public void updateHistory(String login,String login2, Double amount, String receiver, String sender ) {
            try {
                Statement st = con.createStatement();
                
                String query = "UPDATE bankB SET history = CONCAT(history,'," +"Recipient: "+ sender+" Receiver: "+receiver+" Kwota: "+amount+ "') WHERE login = '" + login + "'";
                st.executeUpdate(query);
                 String query1 = "UPDATE bankB SET history = CONCAT(history,'," +"Recipient: "+ receiver+" Receiver: "+sender+" Kwota: "+amount+ "') WHERE login = '" + login2 + "'";
                st.executeUpdate(query1);
               
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
     
     
        } 
     public Boolean updateSaldoIN(String login, Double saldo) {
        Boolean result = false;
        try {
            if (searchUser(login).id == 0) {
                result = false;
            } else {
                Statement st = con.createStatement();
                String query = "UPDATE bankB SET saldo = '" + saldo + "'  WHERE login = '" + login + "'";
                st.executeUpdate(query);
                result = true;
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return result;

    }

    public Boolean updateSaldoOUT(String login, String login2, Double saldo) {
        Boolean result = false;
        try {
            if (searchUser(login).id == 0) {
                result = false;
            } else {
                Statement st = con.createStatement();
                Statement st1 = con.createStatement();
                String query = "INSERT INTO Elixir (nadawca, odbiorca, kwota)" + "VALUES (" + login + "," + login2 + "," + saldo + ")";
                String query1 = "UPDATE bankB SET zablokowane = zablokowane +" + saldo +" WHERE login = '" + login + "'";
                st.executeUpdate(query);
                st1.executeUpdate(query1);
                result = true;
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return result;

    }
        public User searchUser(String login) {
            
             User user = new User(0, 0.0, "", "", "", 0.0);
     
            try {
     
                Statement st = con.createStatement();
                String query = "SELECT * FROM bankB WHERE login = \"" + login + "\"";
                ResultSet rs = st.executeQuery(query);
     
                while (rs.next()) {
     
                    user.id = rs.getInt("id");
                    user.saldo = rs.getDouble("saldo");
                    user.login = rs.getString("login");
                    user.password = rs.getString("password");
                    user.history = rs.getString("history");
                    user.blocked = rs.getDouble("zablokowane");
                }
     
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
     
            return user;
     
        }
     public User searchUserA(String login) {
            
             User user = new User(0, 0.0, "", "", "", 0.0);
     
            try {
     
                Statement st = con.createStatement();
                String query = "SELECT * FROM BankA WHERE login = \"" + login + "\"";
                ResultSet rs = st.executeQuery(query);
     
                while (rs.next()) {
     
                    user.id = rs.getInt("id");
                    user.saldo = rs.getDouble("saldo");
                    user.login = rs.getString("login");
                    user.password = rs.getString("password");
                    user.history = rs.getString("history");
                    user.blocked = rs.getDouble("zablokowane");
                }
     
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
     
            return user;
     
        }
    }