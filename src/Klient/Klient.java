/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Klient;

import Obiekt.Obiekt;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Locale;
import java.util.Scanner;
import java.util.List;
import java.util.Arrays;

/**
 *
 * @author Mateusz
 */
public class Klient {

    private static final String SERVER_IPA = "127.0.0.1";
    private static final int SERVER_PORTA = 5000;
    private static final String SERVER_IPB = "127.0.0.1";
    private static final int SERVER_PORTB = 6000;

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);;
        Socket socket = null;
        String user = null;
        String komunikat = null;
        List<String> historia = null;
        try {
             System.out.println();
                System.out.println("Choose Bank:");
                System.out.println("1 - Bank A");
                System.out.println("2 - Bank B");
            int skan = scanner.nextInt();
            switch(skan)
            {
                case 1:
                    socket = new Socket(SERVER_IPA, SERVER_PORTA);
                     System.out.println("Bank A connected.");
                    break;
                case 2:
                    socket = new Socket(SERVER_IPB, SERVER_PORTB);
                   System.out.println("Bank B connected.");
                    break;
            }
            
        } catch (Exception ex) {
            System.out.println("An error occurred: " + ex.getMessage());
        }

        ObjectInputStream in = null;
        ObjectOutputStream out = null;
        Boolean zalogowany = false;
        while (true) {
            try {
                if (out == null) {
                    out = new ObjectOutputStream(socket.getOutputStream());
                }
                if (in == null) {
                    in = new ObjectInputStream(socket.getInputStream());
                }
                while (zalogowany == false) {
                    System.out.println("Login:");
                    String login = scanner.next();
                    user = login;
                    System.out.println("Password:");
                    String hasło = scanner.next();
                    Integer tagl = 0;
                    out.writeObject(new Obiekt(login, hasło, tagl));
                    out.flush();
                    Obiekt wiad = (Obiekt) in.readObject();
                    komunikat = wiad.getWiadomosc();
                    System.out.println(komunikat);
                    if (komunikat.equals("Welcome!")) {
                        zalogowany = true;
                    }
                }

                System.out.println();
                System.out.println("Options:");
                System.out.println("1 - check your account balance");
                System.out.println("2 - make transfer");
                System.out.println("3 - show history");
                System.out.println("4 - Sign out");


                int tag = scanner.nextInt();

                if (tag == 1) {

                    out.writeObject(new Obiekt(user, tag));
                    Obiekt wiad = (Obiekt) in.readObject();
                    komunikat = wiad.getWiadomosc();
                    System.out.println(komunikat);
                }
                if (tag == 2) {

                    System.out.println("Enter the account id to which you want to transfer money");
                    String odbiorca = scanner.next();
                    System.out.println("Enter how much you want to transfer");
                    Double kasa = scanner.nextDouble();
                    out.writeObject(new Obiekt(user, odbiorca, kasa, tag));
                    out.flush();
                    Obiekt wiad = (Obiekt) in.readObject();
                    komunikat = wiad.getWiadomosc();

                    if (komunikat.equals("Transaction approved")) {
                        System.out.println(komunikat);
                    } else {
                        System.out.println("Something went wrong:");
                        System.out.println(komunikat);
                    }
                }

                if (tag == 4) {
                    out.writeObject(new Obiekt(tag));
                    out.flush();
                    Obiekt wiad = (Obiekt) in.readObject();
                    komunikat = wiad.getWiadomosc();
                    System.out.println(komunikat);
                    System.exit(0);
                }
                if (tag == 3) {

                    out.writeObject(new Obiekt(user, tag));
                    Obiekt wiad = (Obiekt) in.readObject();
                    komunikat = wiad.getWiadomosc();
                    historia = Arrays.asList(komunikat.split(","));
                    System.out.println("Account history:");
                    for (String next : historia) {

                        System.out.println(next);

                    }

                }
            } catch (Exception ex) {
                System.out.println("Error: " + ex);
            }
        }
    }
}
