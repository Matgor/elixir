Protokół:
 1.	Logowanie (id konta + pass)
 2.	Sprawdzenie salda konta (w wiadomości id konta)
 3.	Przelew ( od kogo (wypelnione z automatu po zalogowaniu), do kogo (w numerze konta     niech się zawiera tez id banku, będzie może latwiej w ten sposób je odróżniać), ile – czyli mamy 3 pola do obsługi  - tutaj albo robimy dodatkowe zmienne w obiekcie albo bawimy się z dzieleniem stringa, chyba lepiej będzie dzielic, bo 
 4.	Log out - zamknięcie wszystkiego
 5.	?

System składa się z N > 2 identycznych systemów (serwerów) bankowych,
obsługujących konta klientów (dla celów tego projektu przyjmijmy, że każdy
bank obsługuje czterech klientów) oraz serwera obsługującego transakcje
międzybankowe.
Każdy klient (właściciel konta) banku posiada środki zgromadzone na
rachunku (dla potrzeb projektu może to być dowolna wartość — losowa
lub stała). Klienci banków mogą składać zlecenia przelewów, kierowane
do klientów swojego banku lub klientów innych banków. Zlecenia te są
gromadzone w systemach (serwerach) bankowych do czasu otwarcia sesji
wymiany zleceń. Otwarcie sesji wymiany zleceń następuje na sygnał wysłany
z serwera obsługującego transakcje międzybankowe. Wtedy poszczególne
transakcje przesyłane są między systemami bankowymi za pośrednictwem
serwera międzybankowego i uaktualniane są stany kont klientów banku.
Rolą serwera obsługującego transakcje międzybankowe jest przekazanie
zleceń przelewu pomiędzy bankami oraz prezentacja sumarycznych przepływów międzybankowych. Przelewy dokonywane w obrębie jednego banku nie
są przekazywane do serwera międzybankowego i są księgowane bez oczekiwania na rozpoczęcie sesji wymiany zleceń. Serwer banku powinien na
bieżąco informować o stanie kont klientów i sumie aktywów przechowywanych przez bank. Po każdej sesji wymiany zleceń serwer bankowy powinien
również informować o sumarycznych kwotach „wychodzących” i „wchodzą-cych” kwotach i bilansie transakcji (różnicy pomiędzy kwotą „wchodzącą” i
„wychodzącą”).
W ramach modelu należy również zaprojektować terminal (klienta) systemu bankowego, który pozwala na składanie zleceń przelewu oraz wyświetlanie stanu konta klienta banku.